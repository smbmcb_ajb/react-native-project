<?php


include "../connection/connection.php";
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
header('Access-Control-Allow-Origin: *');

$input=file_get_contents("php://input");
$decode=json_decode($input, true);
// $qrData = array();
// $qrData = $decode['qrData'];
$qrData = $decode['qrData'];

$qrDataPO = $qrData[0]['PO'];
$qrDataGOODSCODE = $qrData[0]['GOODS_CODE'];
$qrDataINVOICE = $qrData[0]['INVOICE'];
$qrDataDATERECEIVE = $qrData[0]['DATE_RECEIVE'];
$qrDataQTY = $qrData[0]['QTY'];
$qrDataASYLINE = $qrData[0]['ASY_LINE'];
$qrDataSUPPLIER = $qrData[0]['SUPPLIER'];
$qrDataPARTNUMBER = $qrData[0]['PART_NUMBER'];
$qrDataNUMBEROFBOX = $qrData[0]['NUMBER_OF_BOX'];
// $resultArray = array();

$query = sqlsrv_query( $localConn, "SELECT * FROM [Warehouse_Traceability_Scanner] 
WHERE INVOICE ='$qrDataINVOICE'
AND DATE_RECEIVE = '$qrDataDATERECEIVE'
AND GOODS_CODE = '$qrDataGOODSCODE'
AND QTY = '$qrDataQTY'
AND PO = '$qrDataPO'
AND ARCHIVE = '0'", array());

if ($query !== NULL){
    $rows = sqlsrv_has_rows( $query );

    if($rows === true){
        // sleep(5);
        echo '{"message":"duplicate"}';
        return;
    }
$insertQuery = "INSERT INTO [MA_Receiving].[dbo].[Warehouse_Traceability_Scanner](GOODS_CODE, ASSY_LINE, SUPPLIER, PART_NUMBER, QTY, DATE_RECEIVE, INVOICE, ARCHIVE, PO, NUMBER_OF_BOX, LOT_NUMBER)VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
          
$params = array(
    $qrDataGOODSCODE, 
    $qrDataASYLINE, 
    $qrDataSUPPLIER, 
    $qrDataPARTNUMBER, 
    $qrDataQTY, 
    $qrDataDATERECEIVE,
    $qrDataINVOICE,
    '0',
    $qrDataPO,
    $qrDataNUMBEROFBOX,
    $qrData[1]['LOT_NUMBER']);

$stmt = sqlsrv_query( $localConn, $insertQuery, $params);
// sleep(5);
if( $stmt ){
    echo '{"message":"success"}';
    // echo json_encode(data[message] = "success"}, JSON_PRETTY_PRINT);
}
                                
else
{
    echo '{"message":"failed"}';;
}
}

    // echo json_encode($resultArray, JSON_PRETTY_PRINT);
    // echo json_encode($resultArray[0], JSON_PRETTY_PRINT);

sqlsrv_close($localConn);


